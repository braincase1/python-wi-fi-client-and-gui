# Python Client #

* Python code to run a client on a computer that connects to the ESP32 boards on the mice
* Includes user-friendly GUI
* Currently consists of one .py file

# User Manual #
The following are instructions for using the BRAINCASE program.

## Installation ##
1. Download the BRAINCASE_GUI.zip file
2. Unzip the BRAINCASE_GUI.zip file
3. Open the unzipped folder
4. Scroll down through the list of files and double-click BRAINCASE_GUI.exe to start the GUI program
	* You may get a warning about viruses because it is a .exe file, but no malicious code was knowingly compiled with the GUI.
	* 2 windows should now appear: one is the GUI itself and the other is a black box, which is the console. Keep the console open and visible because important information will appear here as certain buttons are pressed.

## Selecting the total number of mice to track ##
1. On the first screen, click on the dropdown menu and select the total number of mice that will participate in this experiment.
	* The allowed number of mice can be from 1 to 4, inclusive.
	* The default number of mice is 1.
2. Click "Confirm".  

* At any time before pressing "Start tracking", you can change the number of mice by pressing the "Edit" button.
	* Then, repeat steps 1 and 2.

After confirming, the appropriate number of panels should show up, labeled "Mouse #", where # will be replaced by a number between 1 and 4.

## Configuring Wi-Fi settings ##
* If you have already configured the Wi-Fi settings previously on all of the boards, you may skip this section. You will know if a board is connected to Wi-Fi if the red LED is on.
* IMPORTANT: Make sure your computer and all boards are connected to the same Wi-Fi network. At the University of Delaware, the computer may be connected to eduroam, while the boards may be registered and connected to UD Devices.

1. Type in the SSID (name) of the Wi-Fi network you want to connect to.
2. If this Wi-Fi network is public and no password is needed, you can leave the "Enter password?" checkbox unchecked.
	* Otherwise, click the checkbox to enable the password text box.
	* Then, type in the password for your Wi-Fi network.
3. Press the "Confirm" button  

* At any time before pressing "Start tracking", you can change the Wi-Fi settings by pressing the "Edit" button.
	* Then, repeat steps 1-3.

After confirming, the "Connect to Wi-Fi" buttons for each mouse should be enabled.

## Connecting each board to Wi-Fi ##
1. Insert the lithium polymer battery into the socket on the board.
2. Plug in one (and only one!) ESP32 board into your computer using a micro USB cable and the CP2104 Friend breakout board.
	* Unless you are programming the board, only plug in the following wires:
		* black -> GND
		* gray -> RX
		* white -> TX
	* Doing otherwise will prevent proper Serial communication with the board
3. Click the "Connect to Wi-Fi" button for the appropriate mouse panel if the red LED on this board is not on.
	* The software will now send the network name and password to the ESP32 board to connect it to the network. During this process, the red LED may blink on and off a couple of times.
	* If the red LED is already on and you believe the board is connected to the same Wi-Fi network as your computer, you may press "Get IP address" instead as a fast alternative to the above step. 
4. Unplug the board from the USB cable.
5. You can use the "LED ON" and "LED OFF" radio buttons to test if the blue LED on the board turns on and off as desired.
	* If this is your first time doing this, your firewall may block the internet connection. If this is the case, change your firewall settings to let the GUI continue.
	* You may notice that it takes some time for the LEDs to turn on/off the first few times you click the radio buttons. You may want to keep toggling between on/off until the speed is to your liking to "warm up" the Wi-Fi response of the board.
6. When satisfied, check the "Ready" checkbox. If the blue LED is not turning on and off, check if the previous steps were followed correctly.
7. Repeat steps 1-6 for up to 4 boards, one for each mouse.

## Starting the mouse recognition software ##
Once all of the "Ready" boxes have been checked, you can press "Start tracking" to start the software. A live video stream will pop up. The blue LEDs on the mice will automatically turn on when a certain mouse behavior is detected from particular mice.

## Reading battery levels ##
You can wirelessly read the battery percentage of the batteries connected to each board by clicking the "Refresh battery percentages" button. This button will be enabled after all "Ready" boxes are checked. The battery percentages do not automatically update in order to allow Wi-Fi communications with the board to focus on controlling the LEDs.

## Future work ##
You may notice two male header pins connected by a jumper on one corner of the board. This is to increase the speed of the GUI by suppressing the boot messages that are printed when the board powers on. Thus, the GUI does not have to wait 5 seconds when certain buttons are clicked. As of now, this has not been implemented and the jumper is optional.